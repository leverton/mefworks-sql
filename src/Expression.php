<?php

namespace mef\Sql;

/**
 * A raw expression.
 */
class Expression extends Parameter
{
    /**
     * @var string
     */
    private string $value;

    /**
     * Constructor
     *
     * @param string $value   A raw SQL string
     */
    public function __construct(string $value)
    {
        $this->value = $value;
    }

    /**
     * Return the raw SQL string.
     *
     * @return string
     */
    public function getValue(): string
    {
        return $this->value;
    }

    /**
     * Return the type of Parameter.
     *
     * @return string
     */
    public function getType(): string
    {
        return 'expression';
    }

    /**
     * Return the raw SQL string.
     *
     * @return string
     */
    public function __toString(): string
    {
        return $this->value;
    }
}
