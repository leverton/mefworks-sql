<?php

namespace mef\Sql;

/**
 * A structure that holds an SQL query (with bound placeholders) and the
 * parameters that go with it.
 */
class PreparedQuery
{
    /**
     * @var string
     */
    private string $sql;

    /**
     * @var array
     */
    private array $values;

    /**
     * Constructor
     *
     * @param string $sql    The SQL string with placeholders
     * @param array  $values The values to use for the placeholders
     */
    public function __construct(string $sql, array $values)
    {
        $this->sql = $sql;
        $this->values = $values;
    }

    /**
     * Return the SQL statement.
     *
     * @return string
     */
    public function getSql(): string
    {
        return $this->sql;
    }

    /**
     * Return the values that are meant to go with the query.
     *
     * @return array
     */
    public function getValues(): array
    {
        return $this->values;
    }
}
