<?php

namespace mef\Sql;

use ArrayIterator;
use Countable;
use IteratorAggregate;
use Traversable;

class GroupBy implements IteratorAggregate, Countable
{
    protected array $data = [];
    protected object $parent;

    public function __construct(object $parent = null)
    {
        $this->parent = $parent;
    }

    public function __invoke(string|Parameter $field, string $direction = 'ASC'): object
    {
        if (!($field instanceof Parameter)) {
            $field = new Field($field);
        }

        $entry = new \stdClass();
        $entry->field = $field;
        $entry->direction = strtoupper($direction) == 'DESC' ? 'DESC' : 'ASC';
        $this->data[] = $entry;

        return $this->parent;
    }

    public function clear(): void
    {
        $this->data = [];
    }

    public function count(): int
    {
        return count($this->data);
    }

    public function asArray(): array
    {
        return $this->data;
    }

    public function getIterator(): Traversable
    {
        return new ArrayIterator($this->data);
    }
}
