<?php

namespace mef\Sql;

use Exception;
use BadMethodCallException;
use InvalidArgumentException;

/**
 * Represents a piece of a where clause.
 *
 * It is composed internally of an array of expressions. Each expression can
 * be either another clause or an SQL expression (such as foo = 42). Normally
 * the WhereClause is only created as part of some other SQL Builder class.
 *
 * To build a query like `WHERE age>42 OR (gender='male' AND mood='angry')`
 *
 * <code>
 * $clause = new WhereClause($this);
 * $clause->
 *    where('age', '>', '42')->
 *    openOrWhere('gender', 'male')->where('mood', 'angry')->close();
 * </code>
 *
 * Note that the LHS defaults to being a field, whereas the RHS defaults to
 * being a bindable value. This behavior can be altered by explicitly passing
 * an instance of `mef\Sql\Value` or `mef\Sql\Field`. If only two
 * parameters are given, the operator is assumed to be '='.
 *
 * As evident by the above example, the class supports chaining function calls.
 * The `close()` method does nothing but returns the parent, so there is no
 * need to balance open/close calls except when wanting to continue chaining at
 * the appropriate level.
 *
 * @property-read array $expressions    The list of expressions
 */
class WhereClause
{
    /**
     * The "owner" or parent of the clause.
     *
     * @var mixed
     */
    protected object $parent;

    /**
     * @var array
     */
    protected array $expressions = [];

    /**
     * Constructor
     *
     * @param mixed $parent   The object that owns the where clause.
     *                        Can be anything, including null.
     */
    public function __construct(object $parent = null)
    {
        $this->parent = $parent;
    }

    /**
     * Return the list of expressions.
     *
     * @return array
     */
    final public function getExpressions(): array
    {
        return $this->expressions;
    }

    /**
     * Returns the parent (owning) object
     *
     * @return object          The object passed to the constructor
     */
    public function close(): object
    {
        return $this->parent;
    }

    /**
     * Casts a string argument per the `$default`
     *
     * @param mixed  $arg      The argument to cast to mef\DB\SQL\Parameter
     * @param string $default  'value' or 'field'
     *
     * @return mef\DB\SQL\Parameter
     */
    private function parseArg(string|Parameter $arg, string $default): Parameter
    {
        if (($arg instanceof Parameter) === false) {
            if ($default == 'value') {
                $arg = new Value($arg);
            } elseif ($default == 'field') {
                $arg = new Field($arg);
            }
        }

        return $arg;
    }

    /**
     * Does the main work for the where() and orWhere() methods.
     *
     * @param string $name  'and' or 'or'
     * @param array $args   the arguments passed to where()
     *
     * @return self
     */
    private function processClause(string $name, array $args): self
    {
        $c = count($args);

        if ($c === 1) {
            if ($args[0] instanceof WhereClause) {
                $this->expressions[] = [
                    'bool' => strtoupper($name),
                    'type' => 'clause',
                    'clause' => $args[0]
                ];
            } elseif ($args[0] instanceof Expression) {
                $this->expressions[] = [
                    'bool' => strtoupper($name),
                    'type' => 'expression',
                    'op' => '',
                    'lvalue' => $args[0],
                    'rvalue' => new Expression(''),
                ];
            } else {
                throw new BadMethodCallException();
            }
        } elseif ($c === 2) {
            $this->expressions[] = [
                'bool' => strtoupper($name),
                'type' => 'expression',
                'op' => '=',
                'lvalue' => $this->parseArg($args[0], 'field'),
                'rvalue' => $this->parseArg($args[1], 'value')
            ];
        } elseif ($c === 3) {
            $args[1] = strtoupper($args[1]);

            if ($args[1] == 'IN' || $args[1] == 'NOT IN') {
                if (!is_array($args[2])) {
                    throw new InvalidArgumentException();
                }

                $rvalues = [];
                foreach ($args[2] as $rvalue) {
                    $rvalues[] = $this->parseArg($rvalue, 'value');
                }

                $this->expressions[] = [
                    'bool' => strtoupper($name),
                    'type' => 'expression',
                    'op' => $args[1],
                    'lvalue' => $this->parseArg($args[0], 'field'),
                    'rvalue' => $rvalues
                ];
            } elseif (in_array($args[1], ['=', '<>', '<', '<=', '>', '>=', 'LIKE', 'NOT LIKE'])) {
                $this->expressions[] = [
                    'bool' => strtoupper($name),
                    'type' => 'expression',
                    'op' => $args[1],
                    'lvalue' => $this->parseArg($args[0], 'field'),
                    'rvalue' => $this->parseArg($args[2], 'value')
                ];
            } else {
                throw new InvalidArgumentException();
            }
        } else {
            throw new InvalidArgumentException();
        }

        return $this;
    }

    /**
     * Add an and-where clause
     *
     * @return self
     */
    public function where(...$args): self
    {
        return $this->processClause('and', $args);
    }

    /**
     * Add an or-where clause
     *
     * @return self
     */
    public function orWhere(...$args): self
    {
        return $this->processClause('or', $args);
    }

    /**
     * Add a nested and-where clause
     *
     * @return \mef\Sql\WhereClause The new nested clause
     */
    public function openWhere(...$args): WhereClause
    {
        $clause = new static($this);
        $clause->where(...$args);

        $this->expressions[] = [
            'bool' => 'AND',
            'type' => 'clause',
            'clause' => $clause
        ];

        return $clause;
    }

    /**
     * Add a nested or-where clause
     *
     * @return \mef\Sql\WhereClause The new nested clause
     */
    public function openOrWhere(...$args): WhereClause
    {
        $clause = new static($this);
        $clause->where(...$args);

        $this->expressions[] = [
            'bool' => 'OR',
            'type' => 'clause',
            'clause' => $clause
        ];

        return $clause;
    }
}
