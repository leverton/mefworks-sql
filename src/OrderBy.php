<?php

namespace mef\Sql;

use ArrayIterator;
use Countable;
use IteratorAggregate;
use stdClass;
use Traversable;

/**
 * A structure that represents an OrderBy.
 */
class OrderBy implements IteratorAggregate, Countable
{
    /**
     * @var array
     */
    protected array $data = [];

    /**
     * @var object
     */
    protected object $parent;

    /**
     * Constructor
     *
     * @param object $parent   The parent or owner of the object.
     */
    public function __construct(object $parent = null)
    {
        $this->parent = $parent;
    }

    /**
     * Add an expression.
     *
     * @param string|\mef\Sql\Parameter  $field
     * @param string                     $direction ASC or DESC
     *
     * @return mixed
     */
    public function addExpression(string|Parameter $field, $direction = 'ASC'): object
    {
        if (($field instanceof Parameter) === false) {
            $field = new Field($field);
        }

        $entry = new stdClass();
        $entry->field = $field;
        $entry->direction = strtoupper($direction) === 'DESC' ? 'DESC' : 'ASC';
        $this->data[] = $entry;

        return $this->parent;
    }

    /**
     * Clear the list of expressions.
     */
    public function clear(): void
    {
        $this->data = [];
    }

    /**
     * Return the number of expressions.
     */
    public function count(): int
    {
        return count($this->data);
    }

    /**
     * Return the list of expressions.
     *
     * @return array
     */
    public function asArray(): array
    {
        return $this->data;
    }

    /**
     * Return an iterator for the expressions.
     *
     * @return \Iterator
     */
    public function getIterator(): Traversable
    {
        return new ArrayIterator($this->data);
    }
}
