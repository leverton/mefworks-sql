<?php

namespace mef\Sql;

/**
 * Add support for Where clauses.
 */
trait WhereTrait
{
    /**
     * @var \mef\Sql\WhereClause|null
     */
    protected ?WhereClause $whereClause;

    /**
     * Return the WhereClause.
     *
     * @return \mef\Sql\WhereClause|null
     */
    public function getWhereClause(): ?WhereClause
    {
        return $this->whereClause;
    }

    /**
     * Set the WhereClause.
     *
     * @param \mef\Sql\WhereClause $clause
     */
    public function setWhereClause(WhereClause $clause)
    {
        $this->whereClause = $clause;
    }

    /**
     * Return a new WhereClause for this object.
     *
     * @return \mef\Sql\WhereClause
     */
    protected function newWhereClause(): WhereClause
    {
        return new WhereClause($this);
    }

    /**
     * Add an AND WhereClause.
     *
     * @return self
     */
    public function where(...$args): self
    {
        $this->whereClause->where(...$args);
        return $this;
    }

    /**
     * Add an OR WhereClause.
     *
     * @return self
     */
    public function orWhere(...$args): self
    {
        $this->whereClause->orWhere(...$args);
        return $this;
    }

    /**
     * Add a nested AND WhereClause.
     *
     * @return \mef\Sql\WhereClause
     */
    public function openWhere(...$args): WhereClause
    {
        $clause = $this->newWhereClause();
        $clause->where(...$args);
        $this->whereClause->where($clause);

        return $clause;
    }

    /**
     * Add a nested OR WhereClause.
     *
     * @return \mef\Sql\WhereClause
     */
    public function openOrWhere(...$args): WhereClause
    {
        $clause = $this->newWhereClause();
        $clause->where(...$args);
        $this->whereClause->orWhere($clause);

        return $clause;
    }
}
