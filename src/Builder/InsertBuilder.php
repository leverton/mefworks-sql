<?php

namespace mef\Sql\Builder;

use mef\Db\Statement\StatementInterface;

/**
 * Build an INSERT query.
 */
class InsertBuilder extends AbstractBuilder
{
    /**
     * @var string
     */
    protected string $tableName;

    /**
     * @var array
     */
    protected array $fields = [];

    /**
     * @var array
     */
    protected array $values = [];

    /**
     * Return the table name.
     *
     * @return string
     */
    public function getTableName(): string
    {
        return $this->tableName;
    }

    /**
     * Return the fields.
     *
     * @return array
     */
    public function getFields(): array
    {
        return $this->fields;
    }

    /**
     * Return the values.
     *
     * @return array
     */
    public function getValues(): array
    {
        return $this->values;
    }

    /**
     * Set the table name.
     *
     * @param  string $tableName
     *
     * @return \mef\Sql\Builder\InsertBuilder
     */
    public function into(string $tableName): self
    {
        $this->tableName = $tableName;
        return $this;
    }

    /**
     * Set the field names.
     *
     * @param  array  $fields
     *
     * @return \mef\Sql\Builder\InsertBuilder
     */
    public function fields(array $fields): self
    {
        $this->fields = array_values($fields);
        return $this;
    }

    /**
     * Set the values.
     *
     * @param  array  $values
     *
     * @return \mef\Sql\Builder\InsertBuilder
     */
    public function values(array $values): self
    {
        $this->values = $this->castValues($values);
        return $this;
    }

    /**
     * Set the field and values.
     *
     * @param  array  $values
     *
     * @return \mef\Sql\Builder\InsertBuilder
     */
    public function namedValues(array $values): self
    {
        $this->fields = array_keys($values);
        $this->values = $this->castValues($values);

        return $this;
    }

    /**
     * Execute the query.
     *
     * @return int Number of affected rows
     */
    public function execute(): int
    {
        $prepared = $this->getWriter()->getPreparedInsert($this);

        return $this->getWriter()->
            getDatabaseDriver()->
            prepare($prepared->getSql(), $prepared->getValues())->execute();
    }

    /**
     * Prepare the query.
     *
     * @param  array  $arguments
     *
     * @return \mef\Db\Statement\StatementInterface
     */
    public function prepare(array $arguments = []): StatementInterface
    {
        return $this->getWriter()->
            getDatabaseDriver()->
            prepare($this->getWriter()->getPreparedInsertSql($this), $arguments);
    }

    /**
     * Return the query as an SQL string.
     *
     * @return string
     */
    public function __toString(): string
    {
        return $this->getWriter()->getInsertSql($this);
    }
}
