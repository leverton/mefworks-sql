<?php

namespace mef\Sql\Builder;

use mef\Db\Statement\StatementInterface;

/**
 * Build a DELETE query.
 */
class DeleteBuilder extends AbstractBuilder
{
    use \mef\Sql\FromTrait;
    use \mef\Sql\LimitTrait;
    use \mef\Sql\WhereTrait;

    /**
     * Constructor.
     */
    public function __construct()
    {
        $this->whereClause = $this->newWhereClause();
    }

    /**
     * Execute the query.
     *
     * @return int  The number of affected rows.
     */
    public function execute(): int
    {
        $prepared = $this->getWriter()->getPreparedDelete($this);

        return $this->getWriter()->
            getDatabaseDriver()->
            prepare($prepared->getSql(), $prepared->getValues())->execute();
    }

    /**
     * Prepare the query.
     *
     * @param  array $arguments
     *
     * @return \mef\Db\Statement\StatementInterface
     */
    public function prepare(array $arguments = []): StatementInterface
    {
        return $this->getWriter()->
            getDatabaseDriver()->
            prepare($this->getWriter()->getPreparedDeleteSql($this), $arguments);
    }

    /**
     * Return the query as an SQL string.
     *
     * @return string
     */
    public function __toString(): string
    {
        return $this->getWriter()->getDeleteSql($this);
    }
}
