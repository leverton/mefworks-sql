<?php

namespace mef\Sql\Builder;

use InvalidArgumentException;
use mef\Db\RecordSet\RecordSetInterface;
use mef\Db\Statement\StatementInterface;
use mef\Sql\Field;
use mef\Sql\Parameter;
use mef\Sql\Join;
use mef\Sql\OrderBy;
use mef\Sql\GroupBy;
use mef\Sql\WhereClause;

/**
 * Build a SELECT query.
 */
class SelectBuilder extends AbstractBuilder
{
    use \mef\Sql\FromTrait;
    use \mef\Sql\WhereTrait;
    use \mef\Sql\LimitTrait;

    /**
     * @var array
     */
    protected array $fields = [];

    /**
     * @var boolean
     */
    protected bool $isDistinct = false;

    /**
     * @var \mef\Sql\GroupBy
     */
    protected GroupBy $groupBy;

    /**
     * @var \mef\Sql\WhereClause
     */
    protected WhereClause $having;

    /**
     * @var \mef\Sql\OrderBy
     */
    protected OrderBy $orderBy;

    /**
     * @var null|int
     */
    protected ?int $offset = null;

    /**
     * @var array
     */
    protected array $joins = [];

    /**
     * Constructor.
     */
    public function __construct()
    {
        $this->whereClause = $this->newWhereClause();
        $this->having = $this->newWhereClause();
        $this->orderBy = new OrderBy($this);
        $this->groupBy = new GroupBy($this);
    }

    /**
     * Cloner.
     */
    public function __clone(): void
    {
        $this->whereClause = clone $this->whereClause;
        $this->orderBy = clone $this->orderBy;
        $this->groupBy = clone $this->groupBy;
        $this->having = clone $this->having;
    }

    /**
     * Get the projected fields.
     *
     * @return array
     */
    public function getFields(): array
    {
        return $this->fields;
    }

    /**
     * Project the given fields.
     *
     * @return \mef\Sql\Builder\SelectBuilder
     */
    public function project(...$args): self
    {
        foreach ($args as $arg) {
            if ($arg instanceof Parameter) {
                $this->fields[] = $arg;
            } else {
                foreach (explode(',', $arg) as $f) {
                    $this->fields[] = new Field($f);
                }
            }
        }

        return $this;
    }

    /**
     * Mark the query as returning only distinct rows.
     *
     * e.g., SELECT DISTINCT ...
     *
     * @return \mef\Sql\Builder\SelectBuilder
     */
    final public function distinct(): self
    {
        $this->isDistinct = true;
        return $this;
    }

    /**
     * Mark the query as returning all rows.
     *
     * This is the opposite of distinct().
     *
     * @return \mef\Sql\Builder\SelectBuilder
     */
    final public function all(): self
    {
        $this->isDistinct = false;
        return $this;
    }

    /**
     * Return whether or not the query is distinct.
     *
     * This only returns true if ->distinct() was previously called on the
     * query. If DISTINCT was manually added as part of a SQL string, then
     * it will not return true.
     *
     * @return boolean
     */
    final public function isDistinct(): bool
    {
        return $this->isDistinct;
    }

    /**
     * Return the GroupBy object.
     *
     * @return \mef\Sql\GroupBy
     */
    final public function getGroupBy(): GroupBy
    {
        return $this->groupBy;
    }

    /**
     * Group by the given field.
     *
     * @param  string|\mef\Sql\Parameter $field
     *
     * @return \mef\Sql\Builder\SelectBuilder
     */
    public function groupBy(string|Parameter $field): self
    {
        $this->groupBy->__invoke($field);
        return $this;
    }

    /**
     * Return the Having object.
     *
     * @return \mef\Sql\WhereClause
     */
    final public function getHaving(): WhereClause
    {
        return $this->having;
    }

    /**
     * Add the given expression to the having clause.
     *
     * @return \mef\Sql\Builder\SelectBuilder
     */
    public function having(...$args): self
    {
        $this->having->where(...$args);
        return $this;
    }

    /**
     * Add the given expression to the having clause, and return the new
     * clause.
     *
     * @return \mef\Sql\WhereClause
     */
    public function openHaving(...$args): WhereClause
    {
        $clause = $this->newWhereClause();
        $clause->where(...$args);
        $this->having->where($clause);

        return $clause;
    }

    /**
     * Return the OrderBy object.
     *
     * @return \mef\Sql\OrderBy
     */
    final public function getOrderBy(): OrderBy
    {
        return $this->orderBy;
    }

    /**
     * Order by the given field.
     *
     * Can be called multiple times to sort by multiple fields.
     *
     * @param  string|\mef\Sql\Parameter $field
     * @param  string                    $direction
     *
     * @return \mef\Sql\Builder\SelectBuilder
     */
    public function orderBy(string|Parameter $field, $direction = 'ASC'): self
    {
        $this->orderBy->addExpression($field, $direction);
        return $this;
    }

    /**
     * Return the offset.
     *
     * @return null|int
     */
    final public function getOffset(): ?int
    {
        return $this->offset;
    }

    /**
     * Set the offset.
     *
     * @param null|int $offset
     *
     * @return \mef\Sql\Builder\SelectBuilder
     */
    final public function setOffset($offset): self
    {
        return $this->offset($offset);
    }

    /**
     * Set the offset.
     *
     * @param  null|int|string $offset
     *
     * @return \mef\Sql\Builder\SelectBuilder
     */
    public function offset(null|int|string $offset): self
    {
        if ($offset === null) {
            $this->offset = null;
        } elseif (!preg_match('/^\s*\d+\s*$/', $offset)) {
            throw new InvalidArgumentException();
        } else {
            $this->offset = (int) trim($offset);
        }

        return $this;
    }

    /**
     * Return the joins.
     *
     * @return array
     */
    public function getJoins(): array
    {
        return $this->joins;
    }

    /**
     * Join the table via inner join.
     *
     * @param string $table
     * @param string $alias
     *
     * @return \mef\Sql\Builder\SelectBuilder
     */
    public function join(string $table, string $alias = ''): self
    {
        $this->joins[] = new Join($this, 'inner', $table, $alias);
        return $this;
    }

    /**
     * Join the table via left join.
     *
     * @param  string $table
     * @param  string $alias
     *
     * @return \mef\Sql\Builder\SelectBuilder
     */
    public function leftJoin(string $table, string $alias = ''): self
    {
        $this->joins[] = new Join($this, 'left', $table, $alias);
        return $this;
    }

    /**
     * Join the table via inner join.
     *
     * @param string $table
     * @param string $alias
     *
     * @return null|\mef\Sql\WhereClause
     */
    public function openJoin(string $table, string $alias = ''): ?WhereClause
    {
        $join = new Join($this, 'inner', $table, $alias);
        $this->joins[] = $join;
        return $join->getWhereClause();
    }

    /**
     * Join the table via left join.
     *
     * @param  string $table
     * @param  string $alias
     *
     * @return null|\mef\Sql\WhereClause
     */
    public function openLeftJoin(string $table, string $alias = ''): ?WhereClause
    {
        $join = new Join($this, 'left', $table, $alias);
        $this->joins[] = $join;
        return $join->getWhereClause();
    }

    /**
     * Return number of rows the query contains with no regard to limits and
     * offsets.
     *
     * i.e., It is equivalent to count($builder->getAll()) if the offsets and
     * limits are ignored.
     *
     * @return int
     */
    public function count(): int
    {
        if (count($this->having->getExpressions()) !== 0) {
            throw new \Exception("Cannot do a count() when a HAVING clause is present.");
        }

        $builder = clone $this;
        $builder->orderBy->clear();
        $builder->offset(null);
        $builder->limit(null);

        $isDistinct = $builder->isDistinct ||
            (isset($builder->fields[0]) && preg_match('/^\s*distinct\s/i', $builder->fields[0]));

        if ($isDistinct || $builder->groupBy->count()) {
            if (!$isDistinct) {
                // @todo: If it is distinct, the query could fail due to duplicate column names.
                //        e.g., SELECT COUNT(1) FROM (SELECT DISTINCT * FROM t t1 JOIN t1 t2) mefcountalias
                //
                $builder->fields = ['1'];
            }

            $sql = 'SELECT COUNT(1) FROM (' . $this->getWriter()->getSelectSql($builder) . ') mefcountalias';
        } else {
            $builder->fields = ['COUNT(*)'];
            $sql = $this->getWriter()->getSelectSql($builder);
        }

        return (int) $this->getWriter()->getDatabaseDriver()->query($sql)->fetchValue();
    }

    /**
     * Execute the query.
     *
     * @return \mef\Db\RecordSet\RecordSetInterface
     */
    public function query(): RecordSetInterface
    {
        $prepared = $this->getWriter()->getPreparedSelect($this);

        return $this->getWriter()->getDatabaseDriver()->prepare($prepared->getSql(), $prepared->getValues())->query();
    }

    /**
     * Prepare the query.
     *
     * @param  array  $arguments
     *
     * @return \mef\Db\Statement\StatementInterface
     */
    public function prepare(array $arguments = []): StatementInterface
    {
        return $this->getWriter()->
            getDatabaseDriver()->
            prepare($this->getWriter()->getPreparedSelectSql($this), $arguments);
    }

    /**
     * Return the query as an SQL string.
     *
     * @return string
     */
    public function __toString(): string
    {
        return $this->getWriter()->getSelectSql($this);
    }
}
