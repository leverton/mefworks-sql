<?php

namespace mef\Sql\Builder;

use mef\Db\Statement\StatementInterface;

/**
 * Build an UPDATE query.
 */
class UpdateBuilder extends AbstractBuilder
{
    use \mef\Sql\LimitTrait;
    use \mef\Sql\FromTrait {
        from as table;
    }
    use \mef\Sql\WhereTrait;

    /**
     * @var array
     */
    protected array $fields = [];

    /**
     * @var array
     */
    protected array $values = [];

    /**
     * Constructor.
     */
    public function __construct()
    {
        $this->whereClause = $this->newWhereClause();
    }

    /**
     * Set the field names.
     *
     * @param  array  $fields
     *
     * @return \mef\Sql\Builder\UpdateBuilder
     */
    public function fields(array $fields): self
    {
        $this->fields = array_values($fields);
        return $this;
    }

    /**
     * Set the values.
     *
     * @param  array  $values
     *
     * @return \mef\Sql\Builder\UpdateBuilder
     */
    public function values(array $values): self
    {
        $this->values = $this->castValues($values);
        return $this;
    }

    /**
     * Set the field names and values.
     *
     * @param  array  $values
     *
     * @return \mef\Sql\Builder\UpdateBuilder
     */
    public function namedValues(array $values): self
    {
        $this->fields = array_keys($values);
        $this->values = $this->castValues($values);

        return $this;
    }

    /**
     * Return the field names.
     *
     * @return array
     */
    public function getFields(): array
    {
        return $this->fields;
    }

    /**
     * Return the field values.
     *
     * @return array
     */
    public function getValues(): array
    {
        return $this->values;
    }

    /**
     * Execute the query.
     *
     * @return int Number of affected rows
     */
    public function execute(): int
    {
        $prepared = $this->getWriter()->getPreparedUpdate($this);

        return $this->getWriter()->getDatabaseDriver()->prepare($prepared->getSql(), $prepared->getValues())->execute();
    }

    /**
     * Prepare the query.
     *
     * @param  array  $arguments
     *
     * @return \mef\Db\Statement\StatementInterface
     */
    public function prepare(array $arguments = []): StatementInterface
    {
        return $this->getWriter()->
            getDatabaseDriver()->
            prepare($this->getWriter()->getPreparedUpdateSql($this), $arguments);
    }

    /**
     * Return the query as an SQL string.
     *
     * @return string
     */
    public function __toString(): string
    {
        return $this->getWriter()->getUpdateSql($this);
    }
}
