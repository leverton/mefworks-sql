<?php

namespace mef\Sql\Builder;

use mef\Sql\Writer\AbstractWriter;

/**
 * An interface for SQL builders.
 */
interface BuilderInterface
{
    /**
     * Return the writer.
     *
     * @return \mef\Sql\Writer\AbstractWriter
     */
    public function getWriter(): AbstractWriter;

    /**
     * Set the SQL writer for the builder.
     *
     * @param \mef\Sql\Writer\AbstractWriter $writer
     */
    public function setWriter(AbstractWriter $writer): void;
}
