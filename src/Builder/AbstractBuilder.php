<?php

namespace mef\Sql\Builder;

use mef\Sql\Writer\AbstractWriter;
use mef\Sql\Parameter;
use mef\Sql\Value;
use Stringable;

/**
 * An abstract class with helper methods for builders.
 */
abstract class AbstractBuilder implements BuilderInterface, Stringable
{
    /**
     * @var \mef\Sql\Writer\AbstractWriter
     */
    private AbstractWriter $writer;

    /**
     * Return the writer.
     *
     * @return \mef\Sql\Writer\AbstractWriter
     */
    public function getWriter(): AbstractWriter
    {
        return $this->writer;
    }

    /**
     * Set the writer.
     *
     * @param \mef\Sql\Writer\AbstractWriter $writer
     */
    public function setWriter(AbstractWriter $writer): void
    {
        $this->writer = $writer;
    }

    /**
     * Cast the values to the appropriate Sql\Parameter subtype.
     *
     * @param  array  $values
     * @return array
     */
    protected function castValues(array $values): array
    {
        $parameters = [];

        foreach ($values as $value) {
            if ($value instanceof Parameter) {
                $parameters[] = $value;
            } else {
                $parameters[] = new Value($value);
            }
        }

        return $parameters;
    }

    abstract public function __toString(): string;
}
