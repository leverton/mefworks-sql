<?php

namespace mef\Sql\Driver;

use mef\Db\Driver\AbstractDecoratorDriver;
use mef\Sql\Writer\AbstractWriter;
use mef\Sql\Builder\SelectBuilder;
use mef\Sql\Builder\InsertBuilder;
use mef\Sql\Builder\UpdateBuilder;
use mef\Sql\Builder\DeleteBuilder;

/**
 * A database driver with extended functionality to create SQL builder objects.
 */
class SqlDriver extends AbstractDecoratorDriver implements SqlDriverInterface
{
    /**
     * @var \mef\Sql\Writer\AbstractWriter
     */
    private $writer;

    /**
     * Constructor
     *
     * @param \mef\Sql\Writer\AbstractWriter $writer
     */
    public function __construct(AbstractWriter $writer)
    {
        $this->writer = $writer;
        parent::__construct($writer->getDatabaseDriver());
    }

    /**
     * Return a new SelectBuilder.
     *
     * @param  string $params
     *
     * @return \mef\Sql\Builder\SelectBuilder
     */
    public function select(...$params): SelectBuilder
    {
        $select = new SelectBuilder();
        $select->setWriter($this->writer);
        $select->project(...$params);

        return $select;
    }

    /**
     * Return a new InsertBuilder.
     *
     * @return \mef\Sql\Builder\InsertBuilder
     */
    public function insert(): InsertBuilder
    {
        $insert = new InsertBuilder();
        $insert->setWriter($this->writer);

        return $insert;
    }

    /**
     * Return a new UpdateBuilder.
     *
     * @return \mef\Sql\Builder\UpdateBuilder
     */
    public function update(): UpdateBuilder
    {
        $update = new UpdateBuilder();
        $update->setWriter($this->writer);

        return $update;
    }

    /**
     * Return a new DeleteBuilder.
     *
     * @return \mef\Sql\Builder\DeleteBuilder
     */
    public function delete(): DeleteBuilder
    {
        $delete = new DeleteBuilder();
        $delete->setWriter($this->writer);

        return $delete;
    }
}
