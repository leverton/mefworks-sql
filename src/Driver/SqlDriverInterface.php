<?php

namespace mef\Sql\Driver;

use mef\Db\Driver\DriverInterface;
use mef\Sql\Builder\DeleteBuilder;
use mef\Sql\Builder\InsertBuilder;
use mef\Sql\Builder\SelectBuilder;
use mef\Sql\Builder\UpdateBuilder;

/**
 * A database driver with extended functionality to create SQL builder objects.
 */
interface SqlDriverInterface extends DriverInterface
{
    /**
     * Return a new SelectBuilder.
     *
     * @param  string $params
     *
     * @return \mef\Sql\Builder\SelectBuilder
     */
    public function select(...$params): SelectBuilder;

    /**
     * Return a new InsertBuilder.
     *
     * @return \mef\Sql\Builder\InsertBuilder
     */
    public function insert(): InsertBuilder;

    /**
     * Return a new UpdateBuilder.
     *
     * @return \mef\Sql\Builder\UpdateBuilder
     */
    public function update(): UpdateBuilder;

    /**
     * Return a new DeleteBuilder.
     *
     * @return \mef\Sql\Builder\DeleteBuilder
     */
    public function delete(): DeleteBuilder;
}
