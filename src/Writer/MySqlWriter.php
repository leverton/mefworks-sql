<?php

namespace mef\Sql\Writer;

/**
 * A writer for MySQL.
 */
class MySqlWriter extends AbstractWriter
{
    /**
     * Return the string identifier esacped with the backtick.
     *
     * @param  string $identifier
     * @return string
     */
    public function escapeIdentifier(string $identifier): string
    {
        return "`$identifier`";
    }
}
