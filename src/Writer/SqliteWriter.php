<?php

namespace mef\Sql\Writer;

/**
 * A writer for Sqlite.
 */
class SqliteWriter extends AbstractWriter
{
    /**
     * Return the string identifier esacped with double quotes.
     *
     * @param  string $identifier
     *
     * @return string
     */
    public function escapeIdentifier(string $identifier): string
    {
        return '"' . str_replace('"', '""', $identifier) . '"';
    }
}
