<?php

namespace mef\Sql\Writer;

use Exception;
use InvalidArgumentException;
use mef\Db\Driver\DriverInterface;
use mef\Sql\Builder\SelectBuilder;
use mef\Sql\Builder\InsertBuilder;
use mef\Sql\Builder\DeleteBuilder;
use mef\Sql\Builder\UpdateBuilder;
use mef\Sql\WhereClause;
use mef\Sql\Field;
use mef\Sql\Value;
use mef\Sql\Expression;
use mef\Sql\Parameter;
use mef\Sql\PreparedQuery;

/**
 * An abstract class that implements nearly everything for building SQL
 * strings from a Select, Insert, Update, or Delete builder.
 *
 * Extended classes only need to implement escapeIdentifier.
 */
abstract class AbstractWriter
{
    /**
     * @var \mef\Db\Driver\DriverInterface
     */
    protected DriverInterface $db;

    /**
     * Used as a temporary "global" to store values encountered when building
     * a query. It is reset and discarded after each call to a "write" method.
     *
     * @var array
     */
    private array $placeholderValues;

    /**
     * Constructor
     *
     * @param \mef\Db\Driver\DriverInterface $db
     */
    public function __construct(DriverInterface $db)
    {
        $this->db = $db;
    }

    /**
     * Return the database driver.
     *
     * @return \mef\Db\Driver\DriverInterface
     */
    public function getDatabaseDriver(): DriverInterface
    {
        return $this->db;
    }

    /**
     * Return an escaped version of the identifier.
     *
     * @param  string $identifier
     *
     * @return string
     */
    abstract public function escapeIdentifier(string $identifier): string;

    /**
     * Return an SQL string for the given builder.
     *
     * @param  \mef\Sql\Builder\SelectBuilder $builder
     *
     * @return string
     */
    public function getSelectSql(SelectBuilder $builder): string
    {
        return $this->getSelectSqlForParameterMode($builder, ParameterMode::RAW);
    }

    /**
     * Return a PreparedQuery object so that the SQL can be sent as a true
     * prepared statement to a driver.
     *
     * @param  \mef\Sql\Builder\SelectBuilder $builder
     *
     * @return \mef\Sql\PreparedQuery
     */
    public function getPreparedSelect(SelectBuilder $builder): PreparedQuery
    {
        $sql = $this->getSelectSqlForParameterMode($builder, ParameterMode::INDEXED);

        return new PreparedQuery($sql, $this->placeholderValues);
    }

    /**
     * Return an SQL string for the given builder. All values will be
     * considered to be named parameters, thus the resulting SQL can only be
     * used by following up with a prepare() call along with binding the
     * named values.
     *
     * @param  \mef\Sql\Builder\SelectBuilder $builder
     *
     * @return string
     */
    public function getPreparedSelectSql(SelectBuilder $builder): string
    {
        return $this->getSelectSqlForParameterMode($builder, ParameterMode::NAMED);
    }

    /**
     * Perform the work of building out the SELECT string.
     *
     * Any user supplied value is stored in order in the placeholderValues
     * array.
     *
     * @param  \mef\Sql\Builder\SelectBuilder $builder
     * @param  ParameterMode                  $prepare
     *
     * @return string
     */
    protected function getSelectSqlForParameterMode(SelectBuilder $builder, ParameterMode $prepare = ParameterMode::INDEXED): string
    {
        $this->placeholderValues = [];

        $sql = 'SELECT ';

        if ($builder->isDistinct()) {
            $sql .= 'DISTINCT ';
        }

        if (!count($builder->getFields())) {
            $sql .= '*';
        } else {
            $sql .= implode(',', $builder->getFields());
        }

        $sql .= ' FROM';

        $first = true;
        foreach ($builder->getFromTables() as $alias => $name) {
            if ($first === true) {
                $first = false;
            } else {
                $sql .= ',';
            }

            $sql .= ' ' . $this->escapeIdentifier($name);

            if ($alias != $name) {
                $sql .= ' AS ' . $this->escapeIdentifier($alias);
            }
        }

        foreach ($builder->getJoins() as $join) {
            $sql .= ' ' . strtoupper($join->getType()) . ' JOIN ' . $this->escapeIdentifier($join->getTable());

            if ($join->getAlias()) {
                $sql .= ' AS ' . $join->getAlias();
            }

            if ($join->getWhereClause()->getExpressions()) {
                $sql .= ' ON ' . $this->getWhereSQL($join->getWhereClause());
            }
        }

        if ($builder->getWhereClause()->getExpressions()) {
            $sql .= ' WHERE ' . $this->getWhereSQL($builder->getWhereClause(), $prepare);
        }

        if (count($builder->getGroupBy())) {
            $sql .= ' GROUP BY ';
            $first = true;
            foreach ($builder->getGroupBy() as $groupBy) {
                if ($first) {
                    $first = false;
                } else {
                    $sql .= ', ';
                }

                $sql .= $groupBy->field;
            }
        }

        if ($builder->getHaving()->getExpressions()) {
            $sql .= ' HAVING ' . $this->getWhereSQL($builder->getHaving());
        }

        if (count($builder->getOrderBy())) {
            $sql .= ' ORDER BY ';
            $first = true;
            foreach ($builder->getOrderBy() as $orderBy) {
                if ($first) {
                    $first = false;
                } else {
                    $sql .= ', ';
                }

                $sql .= $orderBy->field . ' ' . $orderBy->direction;
            }
        }

        if ($builder->getLimit()) {
            $sql .= ' LIMIT ' . $builder->getLimit();
        }

        if ($builder->getOffset()) {
            $sql .= ' OFFSET ' . $builder->getOffset();
        }

        return $sql;
    }

    /**
     * Perform the work of building out SQL for a where expression.
     *
     * Any user supplied value is stored in order in the placeholderValues
     * array.
     *
     * @param  \mef\Sql\WhereClause $whereClause
     * @param  ParameterMode        $usePlaceHolders
     *
     * @return string
     */
    protected function getWhereSQL(WhereClause $whereClause, ParameterMode $usePlaceHolders = ParameterMode::RAW): string
    {
        $sql = '';

        foreach ($whereClause->getExpressions() as $expr) {
            if ($sql != '') {
                $sql .= ' ' . $expr['bool'] . ' ';
            }

            switch ($expr['type']) {
                case 'clause':
                    $sql .= '(' . $this->getWhereSQL($expr['clause'], $usePlaceHolders) . ')';
                    break;

                case 'expression':
                    $sql .= $this->getParameterSql($expr['lvalue'], $usePlaceHolders);

                    if ($expr['op'] == 'IN' || $expr['op'] == 'NOT IN') {
                        $sql .= " {$expr['op']} (";
                        $first = true;
                        foreach ($expr['rvalue'] as $rvalue) {
                            if ($first) {
                                $first = false;
                            } else {
                                $sql .= ',';
                            }

                            $sql .= $this->getParameterSql($rvalue, $usePlaceHolders);
                        }
                        $sql .= ')';
                    } elseif (
                        $expr['op'] == '=' &&
                        $expr['rvalue'] instanceof Value &&
                        $expr['rvalue']->getValue() === null
                    ) {
                        $sql .= ' IS NULL';
                    } elseif (
                        $expr['op'] == '<>' &&
                        $expr['rvalue'] instanceof Value &&
                        $expr['rvalue']->getValue() === null
                    ) {
                        $sql .= ' IS NOT NULL';
                    } else {
                        $sql .= " {$expr['op']} ";
                        $sql .= $this->getParameterSql($expr['rvalue'], $usePlaceHolders);
                    }

                    break;

                default:
                    throw new Exception("Unexpected expression of type {$expr['type']}");
            }
        }

        return $sql;
    }

    /**
     * Return an SQL string for the given builder.
     *
     * @param  \mef\Sql\Builder\InsertBuilder $builder
     *
     * @return string
     */
    public function getInsertSql(InsertBuilder $builder): string
    {
        return $this->getInsertSqlForParameterMode($builder);
    }

    /**
     * Return a PreparedQuery object so that the SQL can be sent as a true
     * prepared statement to a driver.
     *
     * @param  \mef\Sql\Builder\InsertBuilder $builder
     *
     * @return \mef\Sql\PreparedQuery
     */
    public function getPreparedInsert(InsertBuilder $builder): PreparedQuery
    {
        $sql = $this->getInsertSqlForParameterMode($builder, ParameterMode::INDEXED);

        return new PreparedQuery($sql, $this->placeholderValues);
    }

    /**
     * Return an SQL string for the given builder. All values will be
     * considered to be named parameters, thus the resulting SQL can only be
     * used by following up with a prepare() call along with binding the
     * named values.
     *
     * @param  \mef\Sql\Builder\InsertBuilder $builder
     *
     * @return string
     */
    public function getPreparedInsertSql(InsertBuilder $builder): string
    {
        return $this->getInsertSqlForParameterMode($builder, ParameterMode::NAMED);
    }

    /**
     * Perform the work of building out the INSERT string.
     *
     * Any user supplied value is stored in order in the placeholderValues
     * array.
     *
     * @param  \mef\Sql\Builder\InsertBuilder $builder
     * @param  ParameterMode                  $valueMode
     *
     * @return string
     */
    private function getInsertSqlForParameterMode(InsertBuilder $builder, ParameterMode $valueMode = ParameterMode::RAW): string
    {
        $this->placeholderValues = [];

        $sql = 'INSERT INTO ' . $this->escapeIdentifier($builder->getTableName()) . ' (';

        $first = true;
        foreach ($builder->getFields() as $field) {
            if ($first === true) {
                $first = false;
            } else {
                $sql .= ',';
            }

            $sql .= $this->escapeIdentifier($field);
        }
        $sql .= ') VALUES (';

        $first = true;
        foreach ($builder->getValues() as $value) {
            if ($first) {
                $first = false;
            } else {
                $sql .= ',';
            }

            $sql .= $this->getParameterSql($value, $valueMode);
        }
        $sql .= ')';

        return $sql;
    }

    /**
     * Return an SQL string for the given builder.
     *
     * @param  \mef\Sql\Builder\UpdateBuilder $builder
     *
     * @return string
     */
    public function getUpdateSql(UpdateBuilder $builder): string
    {
        return $this->getUpdateSqlForParameterMode($builder);
    }

    /**
     * Return a PreparedQuery object so that the SQL can be sent as a true
     * prepared statement to a driver.
     *
     * @param  \mef\Sql\Builder\UpdateBuilder $builder
     *
     * @return \mef\Sql\PreparedQuery
     */
    public function getPreparedUpdate(UpdateBuilder $builder): PreparedQuery
    {
        $sql = $this->getUpdateSqlForParameterMode($builder, ParameterMode::INDEXED);

        return new PreparedQuery($sql, $this->placeholderValues);
    }

    /**
     * Return an SQL string for the given builder. All values will be
     * considered to be named parameters, thus the resulting SQL can only be
     * used by following up with a prepare() call along with binding the
     * named values.
     *
     * @param  \mef\Sql\Builder\UpdateBuilder $builder
     *
     * @return string
     */
    public function getPreparedUpdateSql(UpdateBuilder $builder): string
    {
        return $this->getUpdateSqlForParameterMode($builder, ParameterMode::NAMED);
    }

    /**
     * Perform the work of building out the UPDATE string.
     *
     * Any user supplied value is stored in order in the placeholderValues
     * array.
     *
     * @param  \mef\Sql\Builder\UpdateBuilder $builder
     * @param  ParameterMode                  $valueMode
     *
     * @return string
     */
    private function getUpdateSqlForParameterMode(UpdateBuilder $builder, ParameterMode $valueMode = ParameterMode::RAW): string
    {
        $this->placeholderValues = [];

        $sql = 'UPDATE ' . implode(',', array_map(fn ($i) => $this->escapeIdentifier($i), $builder->getFromTables()));
        $sql .= ' SET ';

        $first = true;

        foreach (array_combine($builder->getFields(), $builder->getValues()) as $field => $value) {
            if ($first === true) {
                $first = false;
            } else {
                $sql .= ',';
            }

            $sql .= $this->escapeIdentifier($field);
            $sql .= '=';
            $sql .= $this->getParameterSql($value, $valueMode);
        }

        $where = $this->getWhereSQL($builder->getWhereClause(), $valueMode);

        if ($where) {
            $sql .= ' WHERE ' . $where;
        }

        if ($builder->getLimit()) {
            $sql .= ' LIMIT ' . $builder->getLimit();
        }

        return $sql;
    }

    /**
     * Return an SQL string for the given builder.
     *
     * @param  \mef\Sql\Builder\DeleteBuilder $builder
     *
     * @return string
     */
    public function getDeleteSql(DeleteBuilder $builder): string
    {
        return $this->getDeleteSqlForParameterMode($builder);
    }

    /**
     * Return a PreparedQuery object so that the SQL can be sent as a true
     * prepared statement to a driver.
     *
     * @param  \mef\Sql\Builder\DeleteBuilder $builder
     *
     * @return \mef\Sql\PreparedQuery
     */
    public function getPreparedDelete(DeleteBuilder $builder): PreparedQuery
    {
        $sql = $this->getDeleteSqlForParameterMode($builder, ParameterMode::INDEXED);

        return new PreparedQuery($sql, $this->placeholderValues);
    }

    /**
     * Return an SQL string for the given builder. All values will be
     * considered to be named parameters, thus the resulting SQL can only be
     * used by following up with a prepare() call along with binding the
     * named values.
     *
     * @param  \mef\Sql\Builder\DeleteBuilder $builder
     *
     * @return string
     */
    public function getPreparedDeleteSql(DeleteBuilder $builder): string
    {
        return $this->getDeleteSqlForParameterMode($builder, ParameterMode::NAMED);
    }

    /**
     * Perform the work of building out the DELETE string.
     *
     * Any user supplied value is stored in order in the placeholderValues
     * array.
     *
     * @param  \mef\Sql\Builder\DeleteBuilder $builder
     * @param  ParameterMode                  $valueMode
     *
     * @return string
     */
    private function getDeleteSqlForParameterMode(DeleteBuilder $builder, ParameterMode $valueMode = ParameterMode::RAW): string
    {
        $this->placeholderValues = [];

        $sql = 'DELETE FROM';

        $first = true;
        foreach ($builder->getFromTables() as $alias => $name) {
            if ($first) {
                $first = false;
            } else {
                $sql .= ',';
            }

            $sql .= ' ' . $this->escapeIdentifier($name);

            if ($alias != $name) {
                $sql .= ' AS ' . $this->escapeIdentifier($alias);
            }
        }

        $sql_where = $this->getWhereSQL($builder->getWhereClause(), $valueMode);

        if ($sql_where) {
            $sql .= ' WHERE ' . $sql_where;
        }

        if ($builder->getLimit() !== null) {
            $sql .= ' LIMIT ' . $builder->getLimit();
        }

        return $sql;
    }

    /**
     * Perform the work of building out the SQL for raw expressions or literals.
     *
     * Any user supplied value is stored in order in the placeholderValues
     * array.
     *
     * @param  \mef\Sql\Parameter $parameter
     * @param  ParameterMode      $valueMode
     *
     * @return string
     */
    protected function getParameterSql(Parameter $parameter, ParameterMode $valueMode = ParameterMode::RAW): string
    {
        if ($parameter instanceof Field) {
            if ($parameter->getTable()) {
                return $this->escapeIdentifier($parameter->getTable()) .
                    '.' . $this->escapeIdentifier($parameter->getField());
            } else {
                return $this->escapeIdentifier($parameter->getField());
            }
        } elseif ($parameter instanceof Value) {
            if ($valueMode === ParameterMode::INDEXED) {
                $this->placeholderValues[] = $parameter->getValue();
                return '?';
            } elseif ($valueMode == ParameterMode::NAMED) {
                $value = $parameter->getValue();

                if (substr($value, 0, 1) !== ':') {
                    throw new InvalidArgumentException('Prepared variable name must begin with colon.');
                }

                return $value;
            } else {
                return $this->quoteValue($parameter->getValue());
            }
        } elseif ($parameter instanceof Expression) {
            return $parameter->getValue();
        } else {
            throw new InvalidArgumentException();
        }
    }

    /**
     * Return a literal.
     *
     * @param  null|string|int|float $value
     *
     * @return string
     */
    public function quoteValue(null|string|int|float $value): string
    {
        return $value === null ? 'null' : $this->db->quoteValue((string) $value);
    }
}
