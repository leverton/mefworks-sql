<?php

namespace mef\Sql\Writer;

enum ParameterMode: string
{
    case RAW = 'raw';
    case INDEXED = 'indexed';
    case NAMED = 'named';
}