<?php

namespace mef\Sql;

/**
 * Add the support for "from" tables.
 */
trait FromTrait
{
    /**
     * An associative array of "alias" => "table name" pairs.
     *
     * @var array
     */
    protected array $fromTables = [];

    /**
     * Return the list of tables.
     *
     * @return array
     */
    public function getFromTables(): array
    {
        return $this->fromTables;
    }

    /**
     * Add a table to the list.
     *
     * If the table alias already exists, then it will overwrite the existing
     * one.
     *
     * @param  string $tableName
     * @param  string $alias
     *
     * @return self
     */
    public function from(string $tableName, string $alias = ''): self
    {
        $this->fromTables[$alias ?: $tableName] = $tableName;

        return $this;
    }
}
