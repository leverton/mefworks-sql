<?php

namespace mef\Sql;

/**
 * Add support for "Limit"
 */
trait LimitTrait
{
    /**
     * @var ?int
     */
    protected ?int $limit = null;

    /**
     * Return the current limit.
     *
     * @return ?int
     */
    public function getLimit(): ?int
    {
        return $this->limit;
    }

    /**
     * Set the limit.
     *
     * @param  ?int $limit
     *
     * @return self
     */
    public function limit(?int $limit): self
    {
        $this->limit = (int) $limit ?: null;
        return $this;
    }
}
