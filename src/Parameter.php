<?php

namespace mef\Sql;

use Stringable;

/**
 * Represents some type of SQL parameter.
 */
abstract class Parameter implements Stringable
{
    /**
     * Return the type of Parameter.
     *
     * @return string
     */
    abstract public function getType(): string;

    /**
     * Return the raw SQL string.
     *
     * This should only be used for debugging purposes. There is no way for it
     * to know the proper way to present the data without the context of the
     * database connection.
     *
     * @return string
     */
    abstract public function __toString(): string;
}
