<?php

namespace mef\Sql;

/**
 * A structure to represent a join.
 */
class Join
{
    use WhereTrait;

    /**
     * @var string
     */
    protected string $table;

    /**
     * @var string
     */
    protected string $alias;

    /**
     * @var object
     */
    protected object $parent;

    /**
     * @var string
     */
    protected string $type;

    /**
     * Constructor
     *
     * @param object $parent The parent or owner of the join.
     * @param string $type   The type of join (e.g., INNER, LEFT)
     * @param string $table  The name of the table.
     * @param string $alias  An alias for the table.
     */
    public function __construct(object $parent, string $type, string $table, string $alias = '')
    {
        $this->parent = $parent;
        $this->type = $type;
        $this->table = $table;
        $this->alias = $alias;

        $this->whereClause = new WhereClause($this->parent);
    }

    /**
     * Return the table name.
     *
     * @return string
     */
    public function getTable(): string
    {
        return $this->table;
    }

    /**
     * Return the join type.
     *
     * @return string
     */
    public function getType(): string
    {
        return $this->type;
    }

    /**
     * Return the table alias.
     *
     * @return string
     */
    public function getAlias(): string
    {
        return $this->alias;
    }

    /**
     * Return the parent object.
     *
     * @return object
     */
    public function close(): object
    {
        return $this->parent;
    }
}
