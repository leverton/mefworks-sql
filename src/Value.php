<?php

namespace mef\Sql;

/**
 * A literal value.
 */
class Value extends Parameter
{
    /**
     * @var string
     */
    private ?string $value;

    /**
     * Constructor
     *
     * @param string $value   The literal value.
     */
    public function __construct(?string $value)
    {
        $this->value = $value;
    }

    /**
     * Return the literal value (unescaped).
     *
     * @return string
     */
    public function getValue(): ?string
    {
        return $this->value;
    }

    /**
     * Return the type of Parameter.
     *
     * @return string
     */
    public function getType(): string
    {
        return 'value';
    }

    /**
     * Return the raw SQL string.
     *
     * This should only be used for debugging purposes. There is no way for it
     * to know the proper way to present the data without the context of the
     * database connection.
     *
     * @return string
     */
    public function __toString(): string
    {
        return $this->value !== null ? "'" . $this->value . "'" : '';
    }
}
