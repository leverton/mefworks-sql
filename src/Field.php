<?php

namespace mef\Sql;

/**
 * Represents a table + field name.
 */
class Field extends Parameter
{
    /**
     * @var string
     */
    private string $table = '';

    /**
     * @var string
     */
    private string $field = '';

    /**
     * Constructor
     *
     * If only one parameter, then $table is the table + field name, separated
     * by a period. If there is no period, then it is assumed to be the field
     * name.
     *
     * + ('foo.bar') and ('foo', 'bar') are the same thing: table "foo" and
     * field "bar".
     * + ('bar') and ('', 'bar') are the same thing: field "bar" with no table.
     *
     * If both parameters are specified, then the first is the table name and
     * the second is the field name.
     *
     * @param string $table
     * @param string $field
     */
    public function __construct(string $table, string $field = '')
    {
        if (func_num_args() == 1) {
            $parts = explode('.', $table, 2);

            if (count($parts) == 2) {
                list($this->table, $this->field) = $parts;
            } else {
                $this->table = '';
                $this->field = (string) $table;
            }
        } else {
            $this->table = (string) $table;
            $this->field = (string) $field;
        }
    }

    /**
     * Get the name of the table.
     *
     * Will be an empty string if no table was specified.
     *
     * @return string
     */
    public function getTable(): string
    {
        return $this->table;
    }

    /**
     * Get the name of the field.
     *
     * @return string
     */
    public function getField(): string
    {
        return $this->field;
    }

    /**
     * Return the type of Parameter.
     *
     * @return string
     */
    public function getType(): string
    {
        return 'field';
    }

    /**
     * Return the raw SQL string.
     *
     * This should only be used for debugging purposes. There is no way for it
     * to know the proper way to present the data without the context of the
     * database connection.
     *
     * @return string
     */
    public function __toString(): string
    {
        return $this->table ? $this->table . '.' . $this->field : $this->field;
    }
}
