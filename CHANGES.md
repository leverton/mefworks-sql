mef\Db - CHANGELOG
==================

**Version 1.0.1**, 19-Oct-2015

* Bug fix: don't cast NULL to '' on query builders.

**Version 1.0.0**, 19-Oct-2015

* First release
