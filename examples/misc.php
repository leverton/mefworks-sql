<?php

require __DIR__ . '/../vendor/autoload.php';

$pdo = new PDO('sqlite::memory:');
$pdoDriver = new mef\Db\Driver\PdoDriver($pdo);
$writer = new mef\Sql\Writer\SqliteWriter($pdoDriver);

$db = new mef\Sql\Driver\SqlDriver($writer);

$db->execute('CREATE TABLE foo (x TEXT, y TEXT, z TEXT)');
$db->execute('CREATE TABLE bar (a TEXT, b TEXT, c TEXT)');

$db->insert()->into('foo')->namedValues(['x' => 'a'])->execute();
$db->insert()->into('bar')->namedValues(['a' => 123, 'b' => 123])->execute();

$select = $db->select('x', 'y', 'z')->from('foo')->where('a', 123)->where('b', 123)->openJoin('bar')->where('x', 'a')->close()->limit(10); // ->offset(20);
echo $select, "\n";

var_dump($pdoDriver->query($select)->fetchAll());

var_dump($select->query()->fetchAll());

var_dump($select->count());

$db->update()->table('foo')->namedValues(['z' => 'party'])->execute();

var_dump($select->query()->fetchAll());

$db->delete()->from('foo')->execute();

var_dump($select->count());


$db->insert()->into('foo')->namedValues(['x' => 'a'])->execute();

$prepared = $writer->getPreparedSelect($db->select()->from('foo')->where('x', 'in', ['a','b']));

var_dump($prepared);

var_dump( $pdoDriver->prepare($prepared->getSql(), $prepared->getValues())->query()->fetchAll() );

$st = $db->select()->from('foo')->where('x', 'in', [':foo',':bar'])->prepare([
	':foo' => '1',
	':bar' => 'a',
]);

var_dump($st->query()->fetchAll());

echo $db->select()->from('foo')->where(new mef\Sql\Expression('x = "a"')), PHP_EOL;

var_dump($db->select()->from('foo')->where(new mef\Sql\Expression('x = "a"'))->query()->fetchAll());

echo $db->update()->table('foo')->namedValues(['sort_order' => new mef\Sql\Expression('sort_order + 1'), 'foo' => 'bar'])->where('id', 42), PHP_EOL;

echo $db->insert()->into('foo')->namedValues(['sort_order' => new mef\Sql\Expression('sort_order + 1'), 'foo' => 'bar']), PHP_EOL;
